import RedirectIfLoggedIn from '@/router/guards/RedirectIfLoggedIn'

import Auth from '@/pages/auth/Auth'
import Login from '@/pages/auth/Login'
import Reset from '@/pages/auth/Reset'
import Forgot from '@/pages/auth/Forgot'

const routes = [
  {
    path: '/',
    component: Auth,
    beforeEnter: RedirectIfLoggedIn,
    children: [
      {
        path: '',
        name: 'Login',
        component: Login,
        meta: { bodyClass: 'login' }
      },
      {
        path: 'reset/:token',
        name: 'Reset',
        component: Reset,
        meta: { bodyClass: 'login' }
      },
      {
        path: 'forgot',
        name: 'Forgot',
        component: Forgot,
        meta: { bodyClass: 'login' }
      }
    ]
  }
]

export default routes
